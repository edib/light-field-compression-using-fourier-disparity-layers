function filename = write(LFRef,varargin)
%WRITE Write lightfield 4D(u,v,x,y) data to yuv file
%   [LFSize,yuvSize] = WRITE(LFRef,varargin)

filename = fullfile(pwd,'sequence');

% Create input parser scheme
p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('filename', filename, @ischar);

% Parse arguments
p.parse(varargin{:});
%filename = p.Results.filename;

% Change 5D LF to a collection of 2D frames
frames = LF.toSlices(LFRef);

% Write frames as a YUV sequence
filename = yuv.write(frames,varargin{:});

end