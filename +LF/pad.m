function [LFRef,LFSizePad] = pad(LFRef,blockSize)
%PAD Pad lightfield data
%   [LFRef,LFSizePad] = pad(LFRef,blockSize)

LFSize = size(LFRef);
imgRes  = LFSize(1:2);
imgSize = LFSize(3:end);

imgSizePad = ceil(imgSize/blockSize)*blockSize;
imgSizePad(3:end)=imgSize(3:end);
LFSizePad = [imgRes,imgSizePad];
padSize = LFSizePad-LFSize;
padSizePre = floor(padSize/2);
padSizePost = padSize - padSizePre;

LFRef = padarray(LFRef,padSizePre,'replicate','pre');
LFRef = padarray(LFRef,padSizePost,'replicate','post');
end