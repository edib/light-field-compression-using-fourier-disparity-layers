function [data,cmap,alpha] = fromSubAps(Dir,varargin)
%FROMSUBAPS Read and reconstruct lightfield from subapertures
%   data = fromSubAps(Dir,varargin)

% Parse input parameters
p = inputParser();
p.addParameter('name' , ''    , @ischar);
p.addParameter('ext'  , '.png', @ischar);
p.addParameter('shift', 0     , @isnumeric);
p.parse(varargin{:});

shift = p.Results.shift;
name  = p.Results.name;
ext   = p.Results.ext;

if ~startsWith(ext,'.')
    ext = ['.',ext];
end

% List files in directory
listing = dir(fullfile(Dir,[name,'*',ext]));

% Select files corresponding to given name and extension
[~,filenames,~] = cellfun(@fileparts,{listing.name},'UniformOutput',false);

% Separate filename into tokens following name_u_v convention, u and v are
% angular positions

if isempty(name)
    inds_u_v = cellfun(@(filename) regexp(filename,'(\d*)_(\d*)','tokens'),...
        filenames,'UniformOutput',false);
else
    inds_u_v = cellfun(@(filename) regexp(filename,['^',name,'_(\d*)_(\d*)'],'tokens'),...
        filenames,'UniformOutput',false);
end

% Remove empty cells
empty = cellfun(@isempty,inds_u_v);
inds_u_v = inds_u_v(~empty);
filenames = filenames(~empty);

inds_u_v = cellfun(@(ind_uv) ind_uv{:},inds_u_v,'UniformOutput',false);

% Default output
cmap  = [];
alpha = [];

frames = {};
frames_cmap = {};
frames_alpha = {};

% Read frames and assemble them together according to index
cellfun(@fill_frames,filenames,inds_u_v);

% Fill any missing frame
frames = fill_empty(frames);

% Join frames
data  = LF.fromSlices(frames);

%
switch ext
    case '.png'
        if ~isempty(frames_alpha)
            alpha = LF.fromSlices(frames_alpha);
        else
            alpha = ones('like',data);
        end
        
        % Find unique cmap
        cmap = unique(frames_cmap{~isempty(frames_cmap)});
end

%%
    function fill_frames(filename,ind_u_v)
        fullname = fullfile(Dir,[filename,ext]);
        
        uv = str2double(ind_u_v);
        uv = uv-shift;
        
        switch ext
            case {'.png'}
                [frame,map,alph] = imread(fullname);
                frames      {uv(1),uv(2)} = frame;
                frames_cmap {uv(1),uv(2)} = map;
                frames_alpha{uv(1),uv(2)} = alph;
            case {'.pfm'}
                frame = LF.pfmread(fullname);
                frames{uv(1),uv(2)} = frame;
            case {'.mat'}
                m = matfile(fullname);
                frame = m.disparity;
                frames{uv(1),uv(2)} = frame;
            otherwise
                frame = imread(fullname);
                frames{uv(1),uv(2)} = frame;
        end
    end

    function frames = fill_empty(frames)
        ind_empty = cellfun(@isempty,frames);
        
        if all(ind_empty(:))
            frames = [];
        elseif any(ind_empty(:))
            non_empty_frames = frames(~ind_empty);
            warning('Some (but not all) frames were missing, filling with zeros');
            
            frames_size = cellfun(@size,non_empty_frames,'UniformOutput',false);
            
            if ~isequal(frames_size{:})
                error('Inconsistent size from captured frames');
            else
                replacement = zeros(frames_size{1},'like',non_empty_frames{1});
                frames(ind_empty) = deal({replacement});
            end
        end
    end
end