function data = join(slices,varargin)
%JOIN Join lightfield slices into lightfield data
%   data = join(slices)
%   data = join(slices,dimsToSplit)
%   data = join(slices,dimsToSplit,squeezeGrid)
%   data = join(slices,dimsToSplit,squeezeGrid,squeezeSlices)

validSplitsFcn = @(x) isnumeric(x)||islogical(x);

gridSplits   = true ([1,ndims(slices   )]);
slicesSplits = false([1,ndims(slices{1})]);

defaultSplits = [gridSplits,slicesSplits];

% Parse input arguments
p = inputParser;
p.addOptional('dimsToSplit',   defaultSplits, validSplitsFcn);
p.addOptional('squeezeGrid'  , true,          @islogical);
p.addOptional('squeezeSlices', true,          @islogical);
p.parse(varargin{:});

dimsToSplit   = logical(p.Results.dimsToSplit);
squeezeGrid   = p.Results.squeezeGrid;
squeezeSlices = p.Results.squeezeSlices;

numDims       = numel( dimsToSplit);
numGridDims   =   nnz( dimsToSplit);
numSlicesDims =   nnz(~dimsToSplit);

if squeezeSlices
    szSlices = size(slices{1});
    szSlices(end+1:numSlicesDims) = 1;
    
    nonSplitDims = ones(1,numDims);
    nonSplitDims(~dimsToSplit) = szSlices;
    
    slices = cellfun(@(slice) reshape(slice,nonSplitDims),slices,'UniformOutput',false);
end

if squeezeGrid
    szGrid = size(slices);
    szGrid(end+1:numGridDims) = 1;
    
    splitDims = ones(1,numDims);
    splitDims(dimsToSplit) = szGrid;
    
    slices = reshape(slices,splitDims);
end

data = cell2mat(slices);
end
