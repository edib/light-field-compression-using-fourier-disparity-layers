classdef Viewer < handle
    %VIEWER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetObservable)
        data
        dims = 'xy'
        name = 'LightField'
        range = 'class'
        cv = [1,1]
        cmap = gray
        slice
        clim
        title
    end
    
    properties
        h  = []
        im = []
        ax = []
    end
    
    properties(Hidden)
        validRanges = {'class','slice','data'};
        climClass
        climData
        gv
        perm = [1,2,3,4,5]
        maxView
    end
    
    methods
        function obj = Viewer(data,varargin)
            p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
            
            p.addRequired('data');
            p.addParameter('dims' , obj.dims  ,@ischar);
            p.addParameter('name' , obj.name  ,@ischar);
            p.addParameter('range', obj.range ,@(range) any(strcmpi(range,obj.validRanges)));
            p.addParameter('cv'   , obj.cv    ,@isnumeric);
            p.addParameter('cmap' , obj.cmap  ,@isnumeric);
            
            p.parse(data,varargin{:});
            
            res = p.Results;
            for fname = fieldnames(res)'
                fn = fname{:};
                obj.(fn) = res.(fn);
            end
            
            obj.updateData();
            obj.updateDims();
            
            obj.updateSlice();
            obj.updateCLim();
            obj.updateTitle();
            
            addlistener(obj, 'data' , 'PostSet', @(src,evnt) obj.updateData);
            addlistener(obj, 'dims' , 'PostSet', @(src,evnt) obj.updateDims);
            addlistener(obj, 'name' , 'PostSet', @(src,evnt) obj.updateName);
            addlistener(obj, 'range', 'PostSet', @(src,evnt) obj.updateRange);
            addlistener(obj, 'cv'   , 'PostSet', @(src,evnt) obj.updateCV);
            addlistener(obj, 'cmap' , 'PostSet', @(src,evnt) obj.updateCMap);
            addlistener(obj, 'slice', 'PostSet', @(src,evnt) obj.updateSlice);
            addlistener(obj, 'title', 'PostSet', @(src,evnt) obj.updateTitle);
            addlistener(obj, 'clim' , 'PostSet', @(src,evnt) obj.updateCLim);
            
            obj.view();
        end
        
        function view(obj)
            if ~obj.validFigure()
                obj.h  = figure;
                obj.ax = gca;
                
                obj.im = imagesc(obj.ax,obj.slice);
                obj.ax.Colormap = obj.cmap;
                axis(obj.ax,'image');
                
                obj.h.KeyPressFcn         = @obj.KeyPressCallback;
                obj.h.WindowButtonDownFcn = @obj.ButtonDownCallback;
                obj.h.WindowButtonUpFcn   = @obj.ButtonUpCallback;
            end
            
            obj.updateFigure();
        end
    end
    
    methods(Hidden)
        function updateData(obj)
            if islogical(obj.data)
                data_ = obj.data;
                data_ = uint8(data_);
                obj.data = [];
                obj.data = data_;
            end
            
            obj.gv = arrayfun(@(x) 1:x,size(obj.data),'UniformOutput',false);
            obj.climClass = getrangefromclass(obj.data);
            obj.climData = [min(obj.data(:)) max(obj.data(:))];
            
            obj.updateCV();
        end
        
        function updateDims(obj)
            obj.updatePerm();
            obj.updateCV();
        end
        
        function updatePerm(obj)
            dims_ = [obj.dims,setdiff('uxvy',obj.dims,'stable')];
            [~,perm_] = sort(dims_,'ascend');
            perm_ = perm_([3,4,1,2]);
            perm_(perm_) = 1:4;
            perm_(perm_>2) = perm_(perm_>2)+1;
            perm_ = [perm_(1:2),3,perm_(3:4)];
            obj.perm = perm_;
        end
        
        function updateName(obj)
            obj.updateTitle();
        end
        
        function updateRange(obj)
            obj.updateCLim();
        end
        
        function updateCV(obj)
            obj.maxView = arrayfun(@(x) size(obj.data,x),obj.perm(4:5));
            obj.cv = min(obj.maxView,max([1,1],obj.cv));
            
            obj.updateTitle();
            obj.updateSlice();
            obj.updateCLim();
        end
        
        function updateSlice(obj)
            gv_ = obj.gv;
            ccv = num2cell(obj.cv);
            [gv_{obj.perm(4:5)}] = deal(ccv{:});
            empty_ = cellfun(@isempty,gv_);
            gv_(empty_) = deal({1});
            
            obj.slice = permute(obj.data(gv_{:}),obj.perm);
            
            if obj.validFigure()
                obj.im.CData = obj.slice;
                obj.im.AlphaData = double(any(~isnan(obj.im.CData),3));
            end
        end
        
        function updateCLim(obj)
            switch lower(obj.range)
                case 'class'
                    obj.clim = obj.climClass;
                case 'slice'
                    obj.clim = [min(obj.slice(:)) max(obj.slice(:))];
                case 'data'
                    obj.clim = obj.climData;
            end
            
            if diff(obj.clim)<=0
                obj.clim(2) = obj.clim(1) + diff(getrangefromclass(obj.clim))/2;
            end
            
            if any(isnan(obj.clim))
                obj.clim = [-inf,inf];
            end
            
            if obj.validFigure()
                obj.ax.CLim = obj.clim;
            end
        end
        
        function updateTitle(obj)
            
            coordStr = {':',':',':',':',':'};
            coordStr(obj.perm(4:5)) = arrayfun(@num2str,obj.cv,'UniformOutput',false);
            coordStr = ['(' strjoin(coordStr,','),')'];
            
            obj.title = [obj.name,' ',coordStr];
            
            if obj.validFigure()
                obj.ax.Title.String = obj.title;
            end
        end
        
        function updateCMap(obj)
            if obj.validFigure()
                obj.ax.Colormap = obj.cmap;
            end
        end
        
        function updateFigure(obj)
            if ~obj.validFigure()
                return
            end
            
            obj.im.CData = obj.slice;
            obj.im.AlphaData = double(any(~isnan(obj.im.CData),3));
            obj.ax.CLim = obj.clim;
            obj.ax.Title.String = obj.title;
            obj.ax.Colormap = obj.cmap;
        end
        
        function KeyPressCallback(obj,~,evnt)
            cv_ = obj.cv;
            switch evnt.Key
                case    'uparrow'
                    cv_(1) = cv_(1)-1;
                case  'downarrow'
                    cv_(1) = cv_(1)+1;
                case  'leftarrow'
                    cv_(2) = cv_(2)-1;
                case 'rightarrow'
                    cv_(2) = cv_(2)+1;
                case 'space'
                    rangeInd = find(strcmp(obj.range,obj.validRanges));
                    rangeInd = mod(rangeInd,3)+1;
                    obj.range = obj.validRanges{rangeInd};
            end
            
            obj.cv = min(obj.maxView,max([1,1],cv_));
        end
        
        function ButtonDownCallback(obj,~,~)
            refView = obj.cv;
            refPos = obj.ax.CurrentPoint(1,[2 1]);
            
            obj.h.WindowButtonMotionFcn = ...
                @(src,evnt) (obj.ButtonMotionCallback(src,evnt,refView,refPos));
        end
        
        function ButtonUpCallback(obj,~,~)
            obj.h.WindowButtonMotionFcn = '';
        end
        
        function ButtonMotionCallback(obj,~,~,refView,refPos)
            axSize = [diff(obj.ax.YLim),diff(obj.ax.XLim)];
            axPos = obj.ax.CurrentPoint(1,[2,1]);
            
            switch obj.h.SelectionType
                case 'normal'
                    mv = 2.*(axPos - refPos)./axSize.*obj.maxView;
                    cv_ = refView + round(mv);
                    obj.cv = min(obj.maxView,max([1,1],cv_));
                case 'alt'
            end
        end
        
        function bool = validFigure(obj)
            bool = ~isempty(obj.im) && isvalid(obj.im);
        end
    end
end