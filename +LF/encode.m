function [nbBits,peaksnr] = encode(LFRef,varargin)
%CODEC Summary of this function goes here
%   Detailed explanation goes here

colSpaces  = {'rgb','ycbcr'};
subSamps   = {'400','420','444','422'};
precisions = {'uint8','uint16','single','double'};
refDir = fullfile(pwd,'Ref');
recDir = fullfile(pwd,'Rec');

% Create input parser scheme
p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('inColSpace'    ,colSpaces{1} ,@(x) any(validatestring(x,colSpaces )));
p.addParameter('outColSpace'   ,colSpaces{2} ,@(x) any(validatestring(x,colSpaces )));
p.addParameter('subSamp'       ,subSamps{1}  ,@(x) any(validatestring(x,subSamps  )));
p.addParameter('readPrecision' ,class(LFRef) ,@(x) any(validatestring(x,precisions)));
p.addParameter('writePrecision',class(LFRef) ,@(x) any(validatestring(x,precisions)));
p.addParameter('name'          ,'sequence'   ,@ischar);
p.addParameter('refDir'        ,refDir       ,@ischar);
p.addParameter('recDir'        ,recDir       ,@ischar);
p.addParameter('padding'       ,true         ,@islogical);

% Parse arguments
p.parse(varargin{:});

LFParams        = p.Results;
extraHEVCParams = p.Unmatched;

if ~exist(LFParams.refDir,'dir')
    mkdir(LFParams.refDir);
end

if ~exist(LFParams.recDir,'dir')
    mkdir(LFParams.recDir);
end

% Writing 
LF.write(LFRef,LFParams);

% Convert inputs for HEVC
HEVCParams = LF.LFtoHEVC(LFParams);

% Encode frames using HEVC
[nbBits,peaksnr] = HEVC.encode(HEVCParams,extraHEVCParams);

% Reading
% LFRec = LF.read(LFParams);

end