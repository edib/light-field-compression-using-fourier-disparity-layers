/*
* MATLAB MEX function : converts a matrix of labels into a cell array where
* each cell contains the list of indices of elements assigned to one label value.
* Element indices start form 0 and are ordered by scanning the from 1st to last dimension of the matrix.
* The labels are assumed to be in the range [1 maximum] with the maximum value given as input.
* Values less or equal to 0 are ignored, values higher than maximum throw an error.
*/

#include "mex.h"
#include "matrix.h"
#include <vector>

/*
* Input:
- 0. Matrix of labels.
- 1. Maximum label value.
* Output:
 - Cell Array of elements indices for each label.
*/
void checkInputs(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nlhs != 1)
		mexErrMsgTxt("Incorrect number of output arguments (only one needed).");

	if (nrhs != 2)
		mexErrMsgTxt("Incorrect number of input arguments (two required).");

	//Check data matrix type
	if (mxGetClassID(prhs[0]) != mxDOUBLE_CLASS)
		mexErrMsgTxt("Matrix in first argument must be of type double.");
	//Check Number of labels value
	if (mxGetClassID(prhs[1]) != mxDOUBLE_CLASS || mxGetNumberOfElements(prhs[1]) != 1)
		mexErrMsgTxt("Second input argument should be a scalar double (number of labels).");

	return;
}



/*
* mex function
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//Inputs:
	checkInputs(nlhs, plhs, nrhs, prhs);

	double *M = (double*)mxGetData(prhs[0]);
	size_t numElts = mxGetNumberOfElements(prhs[0]);

	size_t numSegs = (size_t)mxGetScalar(prhs[1]);

	std::vector<int> NumEltsPerSeg(numSegs, 0);
	int** DataPtrPerSeg = new int*[numSegs];

	size_t dimCellArray[2] = { 1, numSegs };
	mxArray *Cell = mxCreateCellArray(2, dimCellArray);
	plhs[0] = Cell;

	//Loop 1 : Determine number of elements in each label
	for (int i = 0; i < numElts; ++i)
	{
		if (M[i] > 0 && M[i] <= numSegs)
			++NumEltsPerSeg[M[i] - 1];
		else if (M[i] >= numSegs)
			mexErrMsgTxt("Label value higher than the number of segments.");
	}

	//Loop 2 : Allocate memory for the element list of each label
	mxArray* Temp = NULL;
	int skipped = 0;
	size_t dimIdArray[1];
	for (int i = 0; i < numSegs; ++i)
	{
		if (NumEltsPerSeg[i] > 0)
		{
			dimIdArray[0] = NumEltsPerSeg[i];
			Temp = mxCreateNumericArray(1, dimIdArray, mxINT32_CLASS, mxREAL);
			DataPtrPerSeg[i] = (int*)mxGetData(Temp);
			mxSetCell(Cell, i - skipped, Temp);
		}
		else
			++skipped;
	}

	//Loop 3 : Fill the element list of each label
	for (int i = 0; i < numElts; ++i)
	{
		if (M[i] > 0)
			*(DataPtrPerSeg[int(M[i] - 1)]++) = i;
	}

	mxSetN(Cell, numSegs - skipped);

	delete[] DataPtrPerSeg;

	return;

}


