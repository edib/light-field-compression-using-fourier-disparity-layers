/*
* MATLAB MEX function. Given two data matrices, a mask matrix, and segmentation data for each column,
  computes the sum of absolute value error in each segment of each column between the data matrices,
  ignoring the elements with mask value equal to false.
*/

#include "mex.h"
#include "matrix.h"
#include <cmath>

/*
* Input:
- 0. (double) First input Matrix .
- 1. (double) Second input Matrix (both matrices must have the same dimensions).
- 2. (logical) Mask Matrix (false=ignored element).
- 3. Segmentation Cell : each cell contains the data of one segment in one column of the data matrices.
     Segment data consists in the list of line indices of the elements in the segment (must be int32 data type).
- 4. (double) Vector containing the number of segments in each column of the data matrices.

* Output:
- Array of error values of each Segment.
  The error value is computed as the sum of absolute errors of each element divided by the number of elements.
  If all the elements of a segment are masked (false in the mask matrix), the error value is zero.
*/
void checkInputs(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nlhs != 1)
		mexErrMsgTxt("Wrong number of output arguments (1 required).");

	if (nrhs > 5)
		mexErrMsgTxt("Incorrect number of input arguments (5 required).");

	//Check data matrices
	//Dimensions
	mwSize n0 = mxGetNumberOfDimensions(prhs[0]);
	const mwSize* dims0 = mxGetDimensions(prhs[0]);
	mwSize n1 = mxGetNumberOfDimensions(prhs[1]);
	const mwSize* dims1 = mxGetDimensions(prhs[1]);
	mwSize n2 = mxGetNumberOfDimensions(prhs[2]);
	const mwSize* dims2 = mxGetDimensions(prhs[2]);
	if (n0 != n1 || n0 != n2)
		mexErrMsgTxt("Input Matrices must have the same number of dimensions.");
	for (int i = 0; i<n0; ++i) {
		if (dims0[i] != dims1[i] || dims0[i] != dims2[i]) mexErrMsgTxt("Input Matrices must have the same size.");
	}
	// type
	if (mxGetClassID(prhs[0]) != mxDOUBLE_CLASS)
		mexErrMsgTxt("Matrix in 1st input argument must be of type double.");
	if (mxGetClassID(prhs[1]) != mxDOUBLE_CLASS)
		mexErrMsgTxt("Matrix in 2nd input inputargument must be of type double.");
	if (mxGetClassID(prhs[2]) != mxLOGICAL_CLASS)
		mexErrMsgTxt("Mask Matrix (3rd input argument) must be of class logical.");

	//Check Cell of Segmentation data (only check it is of type cell at this point)
	if (!mxIsCell(prhs[3]))
		mexErrMsgTxt("4th input argument must be a cell array.");

	// Check the vector of segment numbers
	// number of dimensions
	if (mxGetM(prhs[4]) > 1 && mxGetN(prhs[4]) > 1)
		mexErrMsgTxt("4th input argument must be a vector.");
	// number of elements
	if (mxGetNumberOfElements(prhs[4]) != dims0[1])
		mexErrMsgTxt("The length of the vector in 5th input argument be equal to the number of columns of the input matrices.");
	// type : double (only accept doubles for simplicity)
	if (mxGetClassID(prhs[4]) != mxDOUBLE_CLASS)
		mexErrMsgTxt("Vector in 5th input argument must be of class double.");

	return;
}



/*
* mex function
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//Inputs:
	checkInputs(nlhs, plhs, nrhs, prhs);

	mwSize ndims = mxGetNumberOfDimensions(prhs[0]);
	const mwSize* dims = mxGetDimensions(prhs[0]);
	double *m1 = (double*)mxGetData(prhs[0]);
	double *m2 = (double*)mxGetData(prhs[1]);
	bool *mask = (bool*)mxGetData(prhs[2]);

	const mxArray* cellSeg = prhs[3];
	size_t nbSegTot = mxGetNumberOfElements(cellSeg);

	double* nbSegPerCol = mxGetPr(prhs[4]);


	plhs[0] = mxCreateDoubleMatrix(1, nbSegTot, mxREAL);
	double* outArray = mxGetPr(plhs[0]);

	//Initialize variables:
	double sumAbsErr;
	int j, k, elId, elListId, s, numValid, jStep=0, sStart=0, sEnd;
	size_t numEltInSeg;
	const mxArray* segEltList=NULL;
	int* segEltListData=NULL;
	mwSize num_i = dims[0], num_j = dims[1], num_k = 0;
	mwSize num_ij = num_i * num_j;

	if (ndims > 2) {
		for (int dim = 2; dim < ndims; ++dim)
			num_k += dims[dim];
	}
	else num_k = 1;


	//Main Loop
	for (j = 0; j < num_j; ++j)//loop on columns
	{
		sEnd = sStart + nbSegPerCol[j];
		if (sEnd > nbSegTot) mexErrMsgTxt("The sum of the values in the vector in 5th input argument should not be higher than the number of cells in the cell array (4th input argument).");
		for (s = sStart; s < sEnd; ++s)//loop on segments of the column
		{
			segEltList = mxGetCell(cellSeg, s);
			if (mxGetClassID(segEltList) != mxINT32_CLASS) mexErrMsgTxt("Each cell of the cell array (4th input argument) must contain data of type int32");
			numEltInSeg = mxGetNumberOfElements(segEltList);
			segEltListData = (int*) mxGetData(segEltList);
			sumAbsErr = 0;
			numValid = 0;
			for (elListId = 0; elListId < numEltInSeg; ++elListId)//loop on line number of the segment
			{
				elId = (int)(segEltListData[elListId] + jStep);
				for (k = 0; k < num_k; ++k)//loop on the dimensions higher than 2
				{
					if (mask[elId])
					{
						sumAbsErr += abs(m1[elId] - m2[elId]);
						++numValid;
					}
					elId += num_ij;
				}
			}
			if (numValid > 0)
				outArray[s] = sumAbsErr / numValid; //TODO divide by number of non-skipped elts (compute it)
			else
				outArray[s] = 0;
		}
		jStep += num_i;
		sStart = s;
	}

	return;
}


