function compile_x64(filename,debug)
clear mex;

debugOpt = '';
if(exist('debug','var') && debug==true)
    debugOpt = '-g';
end

src_dir = fileparts(which('compile_x64.m'));
output_dir =    [src_dir '\x64'];
include_dir =   ['-I" ' src_dir '/include"'];
lib_dir =       ['-L" ' src_dir '/libs/x64"'];
libs = '';


%options=['-g -largeArrayDims -outdir ' output_dir ' ' include_dir  ' ' lib_dir ' ' libs];
%mex(['COMPFLAGS="$COMPFLAGS ' options '"'], [src_dir '\' filename]);
mex(debugOpt, '-largeArrayDims', include_dir, lib_dir, libs, [src_dir '\' filename], '-outdir', output_dir);

clear mex;