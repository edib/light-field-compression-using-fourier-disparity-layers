/*
* MATLAB MEX function. Given a mask matrix, segmentation data for each column, and a list of segments,
  set to false the elements belonging to the segments of the list.
*/

#include "mex.h"
#include "matrix.h"

/*
* Input:
- 0. (logical) Original Mask Matrix.
- 1. Segmentation Cell : each cell contains the data of one segment in one column of matrix.
	 Segment data consists in the list of line indices of the elements in the segment (must be int32 data type).
- 2. (int32) Vector containing the column index of each segment in the Segmentation Cell (indices starting at 1).
- 3. (double) Vector containing indices of segments in the Segmentation cell (indices starting at 1).

* Output:
- Modified Mask matrix.
*/
void checkInputs(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nlhs != 1)
		mexErrMsgTxt("Wrong number of output arguments (1 required).");

	if (nrhs > 4)
		mexErrMsgTxt("Incorrect number of input arguments (4 required).");

	//Check Mask matrix type
	if (mxGetClassID(prhs[0]) != mxLOGICAL_CLASS)
		mexErrMsgTxt("Matrix in 1st input argument must be of class logical.");

	//Check Cell of Segmentation data (only check it is of type cell at this point)
	if (!mxIsCell(prhs[1]))
		mexErrMsgTxt("2nd input argument must be a cell array.");

	// Check the vector of column indices
	// number of dimensions
	if (mxGetM(prhs[2]) > 1 && mxGetN(prhs[2]) > 1)
		mexErrMsgTxt("3rd input argument must be a vector.");
	// number of elements
	if (mxGetNumberOfElements(prhs[2]) != mxGetNumberOfElements(prhs[1]))
		mexErrMsgTxt("Cell array in 2nd input argument and vector in 3rd input argument must have the same length.");
	// type : only accept int32 for simplicity
	if (mxGetClassID(prhs[2]) != mxINT32_CLASS)
		mexErrMsgTxt("Vector in 3rd input argument must be of class int32.");

	// Check the vector of segment indices
	// number of dimensions
	if (mxGetM(prhs[3]) > 1 && mxGetN(prhs[3]) > 1)
		mexErrMsgTxt("4th input argument must be a vector.");
	// type : only accept int32 for simplicity
	if (mxGetClassID(prhs[3]) != mxINT32_CLASS)
		mexErrMsgTxt("Vector in 4th input argument must be of class int32.");

	return;
}



/*
* mex function
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//Inputs:
	checkInputs(nlhs, plhs, nrhs, prhs);

	mwSize ndims = mxGetNumberOfDimensions(prhs[0]);
	const mwSize* dims = mxGetDimensions(prhs[0]);
	bool *maskIn = (bool*)mxGetData(prhs[0]);

	const mxArray* cellSeg = prhs[1];
	size_t nbSegTot = mxGetNumberOfElements(cellSeg);

	int* ColIdPerSeg = (int*)mxGetData(prhs[2]);

	int* SegIdList = (int*)mxGetData(prhs[3]);
	size_t nbSegList = mxGetNumberOfElements(prhs[3]);

	plhs[0] = mxDuplicateArray(prhs[0]);
	bool* outArray = (bool*)mxGetData(plhs[0]);

	//Initialize variables:
	int k, elId, elListId, s, sIdList;
	size_t numEltInSeg;
	const mxArray* segEltList = NULL;
	int* segEltListData = NULL;
	mwSize num_i = dims[0], num_j = dims[1], num_k = 0;
	mwSize num_ij = num_i * num_j;

	if (ndims > 2) {
		for (int dim = 2; dim < ndims; ++dim)
			num_k += dims[dim];
	}
	else num_k = 1;


	//Main Loop
	for (sIdList = 0; sIdList < nbSegList; ++sIdList)//loop on segments in the list
	{
		s = SegIdList[sIdList]-1;
		if (s<0 || s>=nbSegTot) mexErrMsgTxt("Segment indices in 4th input argument must be in the range of the input Segment data.");
		if (ColIdPerSeg[s]<1 || ColIdPerSeg[s]>num_j)
			mexErrMsgTxt("Column indices in 3rd input argument must be in the range of columns of the input matrix.");
		segEltList = mxGetCell(cellSeg, s);
		if (mxGetClassID(segEltList) != mxINT32_CLASS) mexErrMsgTxt("Each cell of the cell array (2nd input argument) must contain data of type int32");
		numEltInSeg = mxGetNumberOfElements(segEltList);
		segEltListData = (int*)mxGetData(segEltList);
		for (elListId = 0; elListId < numEltInSeg; ++elListId)//loop on line number of the segment
		{
			elId = (int)(segEltListData[elListId] + (ColIdPerSeg[s] - 1)*num_i);
			for (k = 0; k < num_k; ++k)//loop on the dimensions higher than 2
			{
				outArray[elId] = false;
				elId += num_ij;
			}
		}
	}

	return;
}


