/*
* MATLAB MEX function taking a vector v of positive values and returning the largest number n such 
* that the sum of the n largest elements in the vector is less than or equal to a threshold t.
*/

#include "mex.h"
#include "matrix.h"


/*
* Input:
- 0. Vector v containing positive values in descending order.
- 1. threshold t on the sum of the n largest values.
* Output:
- Largest number n of largest values in v such that their sum is less than t.
*/
void checkInputs(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nlhs > 1)
		mexErrMsgTxt("Too many output arguments (only 1 required)");

	if (nlhs < 1)
		mexErrMsgTxt("No output argument (1 required)");

	if (nrhs != 2)
		mexErrMsgTxt("Incorrect number of input arguments.");

	// Check the number of dimensions of v
	if (mxGetM(prhs[0]) > 1 && mxGetN(prhs[0]) > 1)
		mexErrMsgTxt("First input argument must be a vector (with values in descending order).");

	// Check the type of v
	if (mxGetClassID(prhs[0]) != mxDOUBLE_CLASS)
		mexErrMsgTxt("Vector in first argument must be double (with values in descending order).");

	// Check the type of t
	if (mxGetClassID(prhs[1]) != mxDOUBLE_CLASS || mxGetNumberOfElements(prhs[1]) != 1 )
		mexErrMsgTxt("Sum threshold parameter should be a scalar double.");

	return;
}



/*
* mex function
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
//Inputs:
	checkInputs(nlhs, plhs, nrhs, prhs);

	double *v_data = (double*)mxGetData(prhs[0]);
	int nb_elts = mxGetNumberOfElements(prhs[0]);

	double t = mxGetScalar(prhs[1]);
	if (t < 0) mexErrMsgTxt("t must be positive.");

//Loop:

	double sum = 0;
	int i = 0;

	while( sum <= t && i < nb_elts )
	{
		sum += v_data[i];
		++i;
	}

	//out = mxCreateDoubleMatrix(1, 1, mxREAL);
	//*mxGetPr(out) = i;
	plhs[0] = mxCreateDoubleScalar(i-1);

	return;

}


