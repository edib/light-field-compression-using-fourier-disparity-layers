function  toSubAps(data,Dir,varargin)
%TOSUBAPS Write lightfield as subapertures
%   toSubAps(data,Dir,varargin)

sz = size(data);
sz = sz(4:end);
sz(end+1:2) = 1;
mask = true(sz);
maxValue = [];
if isinteger(data); maxValue = intmax(class(data)); end

p = inputParser();
p.addParameter('name'    , ''       , @ischar);
p.addParameter('shift'   , 0        , @isnumeric);
p.addParameter('mask'    , mask     , @(x) islogical(x)&&isequal(size(x),size(mask)));
p.addParameter('ext'     , 'png'    , @ischar);
p.addParameter('map'     , []       , @(x) isempty(x)||isnumeric(x));
p.addParameter('alpha'   , []       , @(x) isempty(x)||isnumeric(x));
p.addParameter('maxValue', maxValue , @(x) isempty(x)||isnumeric(x));
p.parse(varargin{:});

name  = p.Results.name;
shift = p.Results.shift;
mask  = p.Results.mask;
ext   = p.Results.ext;
map   = p.Results.map;
alpha = p.Results.alpha;
maxValue = p.Results.maxValue;

if ~startsWith(ext,'.'); ext = ['.',ext]; end
if ~exist(Dir,'dir'); mkdir(Dir); end

frames = LF.toSlices(data);

[X,Y] = ndgrid(1:size(frames,1),1:size(frames,2));
X = X+shift; Y = Y+shift;

indices = arrayfun(@(x,y) {num2str(x),num2str(y)},X,Y,'UniformOutput',false);

if isempty(name)
    filenames = cellfun(@(index) strjoin(      index ,'_'),indices,'UniformOutput',false);
else
    filenames = cellfun(@(index) strjoin([name,index],'_'),indices,'UniformOutput',false);
end

filenames = cellfun(@(filename) fullfile(Dir,[filename,ext]),filenames,'UniformOutput',false);

frames = frames(mask);
filenames = filenames(mask);

if ~isempty(alpha)
    frames_alpha = LF.toSlices(alpha);
    frames_alpha = frames_alpha(mask);
end

switch ext
    case '.png'
        if ~isempty(map)
            if ~isempty(alpha)
                fun_write = @(frame,frame_alpha,filename)...
                    imwrite(frame,filename,'Alpha',frame_alpha);
                
                cellfun(fun_write,frames,frames_alpha,filenames);
            else
                fun_write = @(frame,filename) imwrite(frame,map,filename);
                cellfun(fun_write,frames,filenames);
            end
        elseif ~isempty(alpha)
                fun_write = @(frame,frame_alpha,filename)...
                    imwrite(frame,filename,'Alpha',frame_alpha);
                
                cellfun(fun_write,frames,frames_alpha,filenames);
        else    
            fun_write = @(frame,filename) imwrite(frame,filename);
            cellfun(fun_write,frames,filenames);
        end    
    case '.mat'
        for it = 1:numel(frames)
            disparity = frames{it};
            filename = filenames{it};
            save(filename,'disparity');
        end
    case {'.pgm','.ppm'}
        fun_write = @(frame,filename) imwrite(frame,filename,'MaxValue',maxValue);
        
        cellfun(fun_write,frames,filenames);
    case '.pfm'
        warning('implicit conversion to single in pfmwrite');
        
        fun_write = @(frame,filename) LF.pfmwrite(single(frame),filename);
        cellfun(fun_write,frames,filenames);
    otherwise
        fun_write = @(frame,filename) imwrite(frame,filename);
        cellfun(fun_write,frames,filenames);
end

end