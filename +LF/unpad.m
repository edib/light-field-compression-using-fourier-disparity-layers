function LFRef = unpad(LFRef,LFSize)
%UNPAD Remove padding from lightfield data
%   LFRef = unpad(LFRef,LFSize)

LFSizePad = size(LFRef);
padSize = LFSizePad-LFSize;
padSizePre = floor(padSize/2);
padSizePost = padSize - padSizePre;

gv = arrayfun(@(lfsizepad,pre,post) (1+pre):(lfsizepad-post),...
    LFSizePad,padSizePre,padSizePost,'UniformOutput',false);

LFRef = LFRef(gv{:});
end