function slices = toSlices(data,varargin)
%FRAMES Split lightfield data into slices
%   slices = toSlices(data)
%   slices = toSlices(data,dimsToSplit)
%   slices = toSlices(data,dimsToSplit,squeezeGrid)
%   slices = toSlices(data,dimsToSplit,squeezeGrid,squeezeSlices)

validSplitsFcn = @(x) isnumeric(x)||islogical(x);
defaultSplits = [0,0,0,1,1];

% Parse input arguments
p = inputParser;
p.addOptional('dimsToSplit'  , defaultSplits, validSplitsFcn);
p.addOptional('squeezeGrid'  , true         , @islogical);
p.addOptional('squeezeSlices', true         , @islogical);
p.parse(varargin{:});

dimsToSplit = logical(p.Results.dimsToSplit);
squeezeGrid   = p.Results.squeezeGrid;
squeezeSlices = p.Results.squeezeSlices;

% Input size and dimensions
sz = size(data);
sz(end+1:5) = 1;
numDims = numel(sz);

% Infer split chunks sizes
numSplits = sz;
numSplits(~dimsToSplit) = 1;
numSplits(numDims+1:end) = [];

dimDists = arrayfun(@(nsplit,sz) repelem(sz/nsplit,nsplit),...
    numSplits,sz,...
    'UniformOutput',false);

% Split input data into slices
slices = mat2cell(data,dimDists{:});

if squeezeGrid
    gridSize = sz(dimsToSplit);
    gridSize(end+1:2) = 1;
    slices = reshape(slices,gridSize);
end

if squeezeSlices
    SliceSize = sz(~dimsToSplit);
    SliceSize(end+1:2) = 1;
    slices = cellfun(@(s) reshape(s,SliceSize),slices,'UniformOutput',false);
end
end