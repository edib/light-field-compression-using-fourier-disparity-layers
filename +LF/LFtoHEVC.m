function [LFParams,HEVCParams] = LFtoHEVC(refFilename,recFilename,imgSize,imgRes,varargin)
%LFTOHEVC Convert lightfield parameters to HEVC parameters
%   HEVCParams = LFtoHEVC(varargin)

switch imgSize(3)
    case 1
        subSamp = '400';
    case 3
        subSamp = '444';
    otherwise
        error('Unexpected number of channels');
end

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('bitDepth'   , 8       , @isnumeric);
p.addParameter('subSamp'    , subSamp , @ischar);
p.addParameter('inColSpace' , 'rgb'   , @ischar);
p.addParameter('outColSpace', 'ycbcr' , @ischar);
p.addParameter('encode'     , true    , @islogical);
p.addParameter('decode'     , true    , @islogical);

p.parse(varargin{:});

bitDepth = p.Results.bitDepth;
subSamp  = p.Results.subSamp;

LFParams.subSamp     = subSamp;
LFParams.inColSpace  = p.Results.inColSpace;
LFParams.outColSpace = p.Results.outColSpace;
HEVCParams.encode    = p.Results.encode;
HEVCParams.decode    = p.Results.decode;

bitDepth = num2str(bitDepth);

p.addParameter('InputBitDepth'      ,                      bitDepth, @ischar);
p.parse(varargin{:});
p.addParameter('MSBExtendedBitDepth', p.Results      .InputBitDepth, @ischar);
p.parse(varargin{:});
p.addParameter('InternalBitDepth'   , p.Results.MSBExtendedBitDepth, @ischar);
p.parse(varargin{:});
p.addParameter('OutputBitDepth'     , p.Results   .InternalBitDepth, @ischar);
p.parse(varargin{:});

HEVCParams.      InputBitDepth = p.Results.      InputBitDepth;
HEVCParams.MSBExtendedBitDepth = p.Results.MSBExtendedBitDepth;
HEVCParams.   InternalBitDepth = p.Results.   InternalBitDepth;
HEVCParams.     OutputBitDepth = p.Results.     OutputBitDepth;

p.parse(varargin{:});

params2name = @(name,bd,ext) ...
    utils.params2name(name,imgSize,imgRes,subSamp,bd,ext);

HEVCParams.InputFile     = params2name(refFilename,p.Results. InputBitDepth,'yuv' );
HEVCParams.ReconFile     = params2name(recFilename,p.Results.OutputBitDepth,'yuv' );
HEVCParams.BitstreamFile = params2name(recFilename,p.Results. InputBitDepth,'hevc');
HEVCParams.SourceWidth   = num2str(imgSize(2));
HEVCParams.SourceHeight  = num2str(imgSize(1));
HEVCParams.FramesToBeEncoded = num2str(prod(imgRes));
HEVCParams.InputChromaFormat = subSamp;

Unmatched = p.Unmatched;

Fields = fieldnames(Unmatched);
for Field = Fields'
    Field = Field{:};
    
    HEVCParams.(Field) = Unmatched.(Field);
end

end