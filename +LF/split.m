function slices = split(data,varargin)
%SPLIT Split lightfield data into slices
%   slices = split(data)
%   slices = split(data,dimsToSplit)
%   slices = split(data,dimsToSplit,squeezeGrid)
%   slices = split(data,dimsToSplit,squeezeGrid,squeezeSlices)

dimsSize = size(data);
numDims = ndims(data);

defaultSplits = false(1,numDims);
defaultSplits(1:2) = true;

validSplitsFcn = @(x) isnumeric(x)||islogical(x);

% Parse input arguments
p = inputParser;
p.addOptional('dimsToSplit',   defaultSplits, validSplitsFcn);
p.addOptional('squeezeGrid'  , true,          @islogical);
p.addOptional('squeezeSlices', true,          @islogical);
p.parse(varargin{:});

dimsToSplit = logical(p.Results.dimsToSplit);
squeezeGrid   = p.Results.squeezeGrid;
squeezeSlices = p.Results.squeezeSlices;

% Infer split chunks sizes
numSplits = dimsSize;
numSplits(~dimsToSplit) = 1;
numSplits(numDims+1:end) = [];

dimDists = arrayfun(@(nsplit,sz) repelem(sz/nsplit,nsplit),...
    numSplits,dimsSize,...
    'UniformOutput',false);

% Split input data into slices
slices = mat2cell(data,dimDists{:});

if squeezeGrid
    slices = squeeze(slices);
end

if squeezeSlices
    slices = cellfun(@squeeze,slices,'UniformOutput',false);
end
end