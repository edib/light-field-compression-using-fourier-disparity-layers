function filename = write(frames,varargin)
%WRITE Write yuv file from a cell array of frames
%   WRITE(frames,'name',name) use name as yuv filename
%   WRITE(frames,'refDir',refDir) write in directory refDir
%   WRITE(frames,'subSamp',subSamp) specify YUV sampling scheme
%   WRITE(frames,'writePrecision',writePrecision) specify write data type
%   WRITE(frames,'readPrecision',readPrecision) specify read data type

%% Parse input parameters
filename = fullfile(pwd,'sequence');

imgSize = size(frames{1}); imgSize(end+1:3) = 1;
imgRes  = size(frames);
precision = class(frames{1});
[~,bitDepth] = utils.precision(precision);

switch imgSize(3); case 1; subSamp = '400'; case 3; subSamp = '444'; end

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('filename'   , filename, @ischar);
p.addParameter('subSamp'    , subSamp , @ischar);
p.addParameter('bitDepth'   , bitDepth, @isnumeric);

p.addParameter( 'inColSpace', 'rgb'   , @ischar);
p.addParameter('outColSpace', 'ycbcr' , @ischar);

p.addParameter('addInfo'    , true    , @islogical);

p.parse(varargin{:});
params = p.Results;

filename = params.filename;
subSamp  = params.subSamp;
bitDepth = p.Results.bitDepth;

inColSpace  = p.Results.inColSpace;
outColSpace = p.Results.outColSpace;

addInfo = p.Results.addInfo;

precision = ['uint',num2str(max(8,2^ceil(log2(bitDepth))))];

if addInfo
    filename = yuv.params2name(filename,imgSize,imgRes,subSamp,bitDepth);
end

%% Incompatibility
if imgSize(3)~=1 && imgSize(3)~=3
    error('Unexpected number of channels');
end

if imgSize(3)==1 && ~strcmp(subSamp,'400')
    error('Incompatible size and subsampling');
end

if imgSize(3)==3 &&  strcmp(subSamp,'400')
    error('Incompatible size and subsampling');
end

%% Perform color space transform
if ~strcmp(subSamp,'400')
    frames = yuv.convert(frames,outColSpace,inColSpace,bitDepth);
end

%% YUV 420/422: define interpolation sampling grids
[xq,xgv] = deal(1:imgSize(1));
[yq,ygv] = deal(1:imgSize(2));

switch subSamp
    case '422'
        yq = 1:2:imgSize(2);
    case '420'
        xq = 1.5:2:imgSize(1); yq = 1:2:imgSize(2);
end

UV = griddedInterpolant({xgv,ygv},zeros(imgSize(1:2)),'linear','nearest');

%% Write frames iteratively
% Check if save folder exists
convdir  = fileparts(filename);
if ~exist(convdir,'dir')
    mkdir(convdir);
end

fileID = fopen(filename,'w+');

frames = cellfun(@double,frames,'UniformOutput',false);
if strcmp(subSamp,'400')
    cellfun(@writeFrame ,frames,'UniformOutput',false);
else
    cellfun(@writeFrames,frames,'UniformOutput',false);
end

fclose(fileID);

%% Auxiliary functions
% Write a single Y frame assuming 400 sampling scheme
    function writeFrame(frame)
        fwrite(fileID,frame',precision);
    end

% Write Y,U and V frames assuming 422/420 sampling scheme
    function writeFrames(frames)
        writeFrame(frames(:,:,1));
        UV.Values = frames(:,:,2);
        writeFrame(UV({xq,yq}));
        UV.Values = frames(:,:,3);
        writeFrame(UV({xq,yq}));
    end
end