function filename = conventionName(refDir,name,imgSize,subSamp,bitDepth,ext)
%CONVENTIONNAME Summary of this function goes here
%   Detailed explanation goes here

filename = [name,'_',num2str(imgSize(2)),'x',num2str(imgSize(1))];
filename = [filename,'_',subSamp,'p',num2str(bitDepth),'le',ext];
filename = fullfile(refDir,filename);
end