function fun_encode_fdl(RefDir,RecDir,LFName,uRange,vRange,crop,OrderName,QPList,varargin)
%FUN_ENCODE_FDL Perform light field compression using Fourier Disparity Layers
%   
%   Default parameters:
%   RefDir: Input directory containing one folder for each light field
%   RecDir: Output directory
%   LFName: Name of considered light field in RefDir
%   uRange, vRange: angular indices of images to read in folder RefDir/LFName
%   crop: Spatial border crop applied to all images
%   OrderName: Layer configuration
%   QPList: List of HEVC quantization parameters
%
%   Fast Fourier Transform parameters:
%   More details in @FFT/computeFFT.m
%   
%   Fourier Disparity Layers parameters:
%   More details in FDL-Toolbox/FDL toobox/FDL_Construction/ComputeFDL_gpu.m

FDLp = inputParser; FDLp.KeepUnmatched = true; FDLp.StructExpand = true;
FFTp = inputParser; FFTp.KeepUnmatched = true; FFTp.StructExpand = true;

tStart = tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Default FFT parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FFTp.addParameter('gpuInit'       , true       , @islogical);
FFTp.addParameter('padType'       , 'symmetric', @ischar   );
FFTp.addParameter('padSize'       , [15,15]    , @isnumeric);
FFTp.addParameter('Windowing'     , true       , @islogical);
FFTp.addParameter('windowProfile' , 'hann'     , @ischar   );
FFTp.addParameter('linearizeInput', false      , @islogical);
FFTp.addParameter('gammaOffset'   , 0          , @isnumeric);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Default FDL parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FDLp.addParameter('lambdaCalib'    , 1     , @isnumeric);
FDLp.addParameter('lambdaConstruct', .1    , @isnumeric);
FDLp.addParameter('numLayers'      , 30    , @isnumeric);
FDLp.addParameter('flipU'          , false , @(x) (isnumeric(x)||islogical(x)));
FDLp.addParameter('flipV'          , false , @(x) (isnumeric(x)||islogical(x)));

%% Parse FFT and FDL parameters
FFTp.parse(varargin{:});
FDLp.parse(varargin{:});

FFTParams.gpuInit        = FFTp.Results.gpuInit;
FFTParams.padType        = FFTp.Results.padType;
FFTParams.padSize        = FFTp.Results.padSize;
FFTParams.Windowing      = FFTp.Results.Windowing;
FFTParams.windowProfile  = FFTp.Results.windowProfile;
FFTParams.linearizeInput = FFTp.Results.linearizeInput;
FFTParams.gammaOffset    = FFTp.Results.gammaOffset;

FDLParams.lambdaCalib     = FDLp.Results.lambdaCalib;
FDLParams.lambdaConstruct = FDLp.Results.lambdaConstruct;
FDLParams.numLayers       = FDLp.Results.numLayers;
FDLParams.flipU           = FDLp.Results.flipU;
FDLParams.flipV           = FDLp.Results.flipV;

FDLParams.flipU = logical(FDLParams.flipU);
FDLParams.flipV = logical(FDLParams.flipV);

FDLParams.nU = length(uRange);
FDLParams.nV = length(vRange);

%% Load light field
LFRef = loadLF(fullfile(RefDir,LFName), LFName, 'png', uRange, vRange, crop);

LFRef = double(LFRef)/double(intmax(class(LFRef)));

FDLParams.nU = length(uRange);
FDLParams.nV = length(vRange);

%% Infer light field size
sz = size(LFRef);
imgSize = sz(1:2);
nChan = sz(3);
angSize = sz(4:5);

numPix = repmat(prod(imgSize),angSize);
numPix_All = prod(imgSize)*prod(angSize);

if nChan == 3
    subSamp = '444';
else
    subSamp = '400';
end

%% Set HEVC configuration
Intercfg = fullfile(HEVC.Dir,'cfg','encoder_randomaccess_main_rext.cfg');

%% Load layer configuration
configName = [num2str(angSize(1)),'x',num2str(angSize(2)),'_',OrderName];
configName = fullfile('LayerConfigurations',configName);

m = matfile(configName,'Writable',false);
Order    = m.Order;
SubOrder = m.SubOrder;

%% Save input parameters
struct2table(FFTParams)
struct2table(FDLParams)

resuDir = fullfile(RecDir,LFName);
if ~exist(resuDir,'dir'), mkdir(resuDir); end

m_params = matfile(fullfile(resuDir,'initParams.mat'),'Writable',true);
m_params.RefDir = RefDir;
m_params.RecDir = RecDir;
m_params.LFName = LFName;
m_params.uRange = uRange;
m_params.vRange = vRange;
m_params.crop = crop;
m_params.OrderName = OrderName;
m_params.QPList = QPList;
m_params.varargin = varargin;
m_params.FFTParams = FFTParams;
m_params.FDLParams = FDLParams;

%% Save reference
refDir  = fullfile(resuDir,'Reference');
if ~exist(refDir,'dir'), mkdir(refDir); end

LF.toSubAps(LFRef,refDir,'ext','png','name',LFName);

%% Calibration of FDL using input views
FDLObj = FDL(LFRef,FFTParams,FDLParams);
[U,V] = FDLObj.CalibrateFDL([],FDLParams);

%% Run algorithm for different QP values
for QP = QPList
    %% Initialize reconstructed lightfield, results and save directories
    % Light fields
    LFRec = zeros(size(LFRef));
    LFRes = zeros(size(LFRef));
    
    % Results
    scale = nan(prod(angSize),nChan);
    nbBits = nan(angSize);
    bitrate = nan(angSize);
    
    % Directories
    predDir = fullfile(resuDir,['Prediction_',num2str(QP)]);
    if ~exist(predDir,'dir'), mkdir(predDir); end
    predResDir = fullfile(resuDir,['Prediction_Residual_',num2str(QP)]);
    if ~exist(predResDir,'dir'), mkdir(predResDir); end
    recDir  = fullfile(resuDir,['Reconstruction_',num2str(QP)]);
    if ~exist(recDir,'dir'), mkdir(recDir); end
    recResDir = fullfile(resuDir,['Reconstruction_Residual_',num2str(QP)]);
    if ~exist(recResDir,'dir'), mkdir(recResDir); end
    resuMat = fullfile(resuDir,['Results_',num2str(QP),'.mat']);
    
    HEVCRefDir = fullfile(resuDir,['HEVC_Ref_',num2str(QP)]);
    if ~exist(HEVCRefDir,'dir'), mkdir(HEVCRefDir); end
    HEVCRecDir = fullfile(resuDir,['HEVC_Rec_',num2str(QP)]);
    if ~exist(HEVCRecDir,'dir'), mkdir(HEVCRecDir); end
    
    save(resuMat,'imgSize','nChan','angSize','numPix','numPix_All');
    
    %% Prediction + residual coding of current frame layer
    for it = 1:max(Order(:))
        name = [LFName,'_',num2str(it),'_',num2str(QP)];
        
        refFilename = fullfile(HEVCRefDir,name);
        recFilename = fullfile(HEVCRecDir,name);
        
        decodedInd = Order(:)<=it-1;
        currentInd = Order(:)==it;
        
        %% Construction of Fourier Disparity Layers using decoded frames
        if it>1
            rMod = FDLObj.ModelFDL(LFRec(:,:,:,decodedInd(:)),FDLParams,...
                'U',U(decodedInd(:)),'V',V(decodedInd(:)));
        end
        
        %% Prediction of current frame layer using Fourier Disparity Layers
        if it==1
            % No prediction for first layer
            scale(currentInd,:)=1;
            LFRec(:,:,:,currentInd) = LFRef(:,:,:,currentInd);
        else
            % Prediction + color correction
            for ind = find(currentInd)'
                rMod.setPosition(U(ind),V(ind));
                rMod.renderImage();
                
                predIm = double(rMod.Image)/255;
                refIm = LFRef(:,:,:,ind);
                
                refIm = reshape(refIm,[],nChan);
                predIm = reshape(predIm,[],nChan);
                
                for chan = 1:nChan
                    scale(ind,chan) = ...
                        (predIm(:,chan)'*refIm(:,chan))./(predIm(:,chan)'*predIm(:,chan));
                    LFRec(:,:,chan,ind) = min(scale(ind,chan).*reshape(predIm(:,chan),imgSize), 1);
                end
            end
            
            LFRes(:,:,:,currentInd) = LFRef(:,:,:,currentInd)-LFRec(:,:,:,currentInd);
        end
        
        % Save frames
        LF.toSubAps(LFRec,predDir,'name',LFName,...
            'ext','png','mask',reshape(currentInd,angSize));
        
        LF.toSubAps(LFRes/2+0.5,predResDir,'name',LFName,...
            'ext','png','mask',reshape(currentInd,angSize));
        
        if it==1
            LFRec(:,:,:,currentInd) = zeros(size(LFRef(:,:,:,currentInd)));
            LFRes(:,:,:,currentInd) = LFRef(:,:,:,currentInd);
        end
        
        %% Encoding of residual frame layer
        % indices of frames to encode/decode (previous+current)
        if it==1
            residualInd = currentInd;
        else
            residualInd = (decodedInd|currentInd)&Order(:)>1;
        end
        
        % Reorder frames for more efficient compression
        temp = SubOrder(residualInd);
        [~,sig] = sort(temp,'ascend');
        
        residualIndSig = find(residualInd);
        residualIndSig = residualIndSig(sig);
        
        [~,~,curResInd] = intersect(find(currentInd),residualIndSig,'stable');
        
        TempRes = LFRes(:,:,:,residualIndSig);
        
        % Encode/decode frames
        if it==1
            TempRes = uint8(255*TempRes);
            [TempRes,nbBitsRes] = LF.codec(TempRes,...
                'refFilename',refFilename,'recFilename',recFilename,...
                'ConfigFile',Intercfg,...
                'subSamp',subSamp,'QP',num2str(QP),'InputBitDepth','8');
            TempRes = double(TempRes)/255;
        else
            TempRes = uint16(511*TempRes+511);
            [TempRes,nbBitsRes] = LF.codec(TempRes,...
                'refFilename',refFilename,'recFilename',recFilename,...
                'ConfigFile',Intercfg,...
                'subSamp',subSamp,'QP',num2str(QP),'InputBitDepth','10');
            TempRes = (double(TempRes)-511)/511;
        end
        
        % Add decode residual to prediction
        LFRec(:,:,:,currentInd) = LFRec(:,:,:,currentInd)+TempRes(:,:,:,curResInd);
        
        % Save frames
        LF.toSubAps(LFRec,recDir,'name',LFName,...
            'ext','png','mask',reshape(residualInd,angSize));
        
        LF.toSubAps((LFRef-LFRec)/2+0.5,recResDir,'name',LFName,...
            'ext','png','mask',reshape(residualInd,angSize));
        
        % Save bit size and bit rate
        nbBits(residualIndSig) = nbBitsRes;
        bitrate(residualIndSig) = nbBitsRes'./numPix(residualIndSig);
    end
    
    save(resuMat,'scale','nbBits','bitrate','-append');
    
    nbBits_all = sum(nbBits(:));
    bitrate_all = nbBits_all./numPix_All;
    
    save(resuMat,'nbBits_all','bitrate_all','-append');
end

toc(tStart)
end