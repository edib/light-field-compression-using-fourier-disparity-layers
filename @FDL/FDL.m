classdef FDL < handle
    %FDLOBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        numLayers = 20;
        lambdaCalib = 1;
        lambdaConstruct = .1;
        
        FFTObj
        
        U
        V
        D
        
        nU
        nV
        
        FDLObj
        
        rMod
    end
    
    methods
        function obj = FDL(LFRef,varargin)
            p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
            p.addParameter('numLayers'       , obj.numLayers       , @isnumeric);
            p.addParameter('lambdaCalib'     , obj.lambdaCalib     , @isnumeric);
            p.addParameter('lambdaConstruct' , obj.lambdaConstruct , @isnumeric);
            
            p.parse(varargin{:});
            obj.numLayers       = p.Results.numLayers;
            obj.lambdaCalib     = p.Results.lambdaCalib;
            obj.lambdaConstruct = p.Results.lambdaConstruct;

            %% FFT Computation
            obj.ComputeFFT(LFRef,varargin{:});
            
            %% FDL Calibration
            % obj.CalibrateFDL(LFRef,varargin{:});
            
            %% FDL Construction
            % obj.ComputeFDL(LFRef,varargin{:});
            
            %% FDL Model Construction
            % obj.ModelFDL(LFRef,varargin{:});
        end
    end
    
    methods
        FFTObj = ComputeFFT(obj,LFRef,varargin);
        [U,V,D] = CalibrateFDL(obj,LFRef,varargin);
        FDLObj = ComputeFDL(obj,LFRef,varargin);
        rMod = ModelFDL(obj,LFRef,varargin);
    end
    
    methods(Static)
        [U,V,D,sigU,sigV,sigD] = ScaleParams(U,V,D,nU,nV,sigU,sigV,sigD);
        rMod = SetRenderModel(FDL,fullResY,fullResX,padSizeX,padSizeY,D,...
            DispMap,linearizeInput,gammaOffset)
    end
end