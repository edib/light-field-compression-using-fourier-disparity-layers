function rMod = SetRenderModel(FDL,fullResY,fullResX,padSizeX,padSizeY,D,...
    DispMap,linearizeInput,gammaOffset)
%SETRENDERMODEL Summary of this function goes here
%   Detailed explanation goes here

rMod = RenderModel(FDL,[fullResY,fullResX],...
    [padSizeX,padSizeX,padSizeY,padSizeY],D,DispMap,linearizeInput,gammaOffset);
end