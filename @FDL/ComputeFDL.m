function FDLObj = ComputeFDL(obj,LFRef,varargin)

useGPU = parallel.gpu.GPUDevice.isAvailable;

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('U'               , obj.U               , @isnumeric);
p.addParameter('V'               , obj.V               , @isnumeric);
p.addParameter('D'               , obj.D               , @isnumeric);
p.addParameter('lambdaConstruct' , obj.lambdaConstruct , @isnumeric);
p.addParameter('Px'   , []       , @(x) isempty(x)||isnumeric(x));
p.addParameter('Py'   , []       , @(x) isempty(x)||isnumeric(x));

p.parse(varargin{:});
obj.U = p.Results.U;
obj.V = p.Results.V;
obj.D = p.Results.D;
obj.lambdaConstruct = p.Results.lambdaConstruct;
Px = p.Results.Px;
Py = p.Results.Py;

if isempty(obj.U)||isempty(obj.V)||isempty(obj.D)
    obj.CalibrateFDL(LFRef,varargin{:});
elseif ~isempty(LFRef)||isempty(obj.FFTObj)
    obj.ComputeFFT(LFRef,varargin{:});
end

tic;fprintf('FDL Construction...');

if useGPU
    FDLObj = ComputeFDL_gpu(obj.FFTObj.ViewsFFT,obj.FFTObj.wx,obj.FFTObj.wy,...
        obj.U,obj.V,obj.D,obj.lambdaConstruct,Px,Py);
else
    FDLObj = ComputeFDL_cpu(obj.FFTObj.ViewsFFT,obj.FFTObj.wx,obj.FFTObj.wy,...
        obj.U,obj.V,obj.D,obj.lambdaConstruct,Px,Py);
end

t=toc;fprintf(['(' num2str(t) 's)\n']);

obj.FDLObj = FDLObj;

end