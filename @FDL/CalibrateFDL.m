function [U,V,D] = CalibrateFDL(obj,LFRef,varargin)

if ~isempty(LFRef)||isempty(obj.FFTObj)
    obj.ComputeFFT(LFRef,varargin{:});
end

numInitViews = size(obj.FFTObj.ViewsFFT,1);
PositInit = zeros(numInitViews,1,'single');
DispsInit = linspace(0,10,obj.numLayers);

useGPU = parallel.gpu.GPUDevice.isAvailable;

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('numLayers'   , obj.numLayers   , @isnumeric);
p.addParameter('lambdaCalib' , obj.lambdaCalib , @isnumeric);
p.addParameter('U'           , PositInit       , @isnumeric);
p.addParameter('V'           , PositInit       , @isnumeric);
p.addParameter('D'           , DispsInit       , @isnumeric);
p.addParameter('flipU'       , false           , @islogical);
p.addParameter('flipV'       , false           , @islogical);

p.parse(varargin{:});
obj.numLayers   = p.Results.numLayers;
obj.lambdaCalib = p.Results.lambdaCalib;

tic;fprintf('FDL Calibration...');

if useGPU
    [~,~,U,V,D,~,~,~,~,~,~,~] = CalibrateFDL_UVD_gpu(...
        obj.FFTObj.ViewsFFT,obj.FFTObj.wx,obj.FFTObj.wy,...
        obj.numLayers,obj.lambdaCalib,...
        p.Results.U,p.Results.V,p.Results.D);
else
    [~,~,U,V,D,~,~,~,~,~,~,~] = CalibrateFDL_UVD_cpu(...
        obj.FFTObj.ViewsFFT,obj.FFTObj.wx,obj.FFTObj.wy,...
        obj.numLayers,obj.lambdaCalib,...
        p.Results.U,p.Results.V,p.Results.D);
end

nU = 2*max(abs(U(:)-mean(U(:))));
nV = 2*max(abs(V(:)-mean(V(:))));

p.addParameter('nU' , nU , @isnumeric);
p.addParameter('nV' , nV , @isnumeric);

p.parse(varargin{:});

obj.nU = p.Results.nU;
obj.nV = p.Results.nV;

[U,V,D] = FDL.ScaleParams(U,V,D,obj.nU,obj.nV);

t=toc;fprintf(['(' num2str(t) 's)\n']);

if p.Results.flipU
    tic;fprintf('Flipping U Coordinates...\n');
    U = -U;
end

if p.Results.flipV
    tic;fprintf('Flipping V Coordinates...\n');
    V = -V;
end

if p.Results.flipU || p.Results.flipV
    tic;fprintf('FDL Calibration, again...\n');
    if useGPU
        [~,~,U,V,D,~,~,~,~,~,~,~] = CalibrateFDL_UVD_gpu(...
            obj.FFTObj.ViewsFFT,obj.FFTObj.wx,obj.FFTObj.wy,...
            obj.numLayers,obj.lambdaCalib,...
            U,V,D);
    else
        [~,~,U,V,D,~,~,~,~,~,~,~] = CalibrateFDL_UVD_cpu(...
            obj.FFTObj.ViewsFFT,obj.FFTObj.wx,obj.FFTObj.wy,...
            obj.numLayers,obj.lambdaCalib,...
            U,V,D);
    end

    [U,V,D] = FDL.ScaleParams(U,V,D,obj.nU,obj.nV);
end

obj.U = U;
obj.V = V;
obj.D = D;

end