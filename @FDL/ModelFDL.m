function rMod = ModelFDL(obj,LFRef,varargin)
%SETRENDERMODEL Summary of this function goes here
%   Detailed explanation goes here

if ~isempty(LFRef)||isempty(obj.FDLObj)
    obj.ComputeFDL(LFRef,varargin{:});
end

tic;fprintf('FDL Model Construction...');

dummyDispMap = zeros([size(LFRef,1),size(LFRef,2)],'single');
rMod = FDL.SetRenderModel(obj.FDLObj,obj.FFTObj.fullResY,obj.FFTObj.fullResX,...
    obj.FFTObj.padSizeX,obj.FFTObj.padSizeY,obj.D,dummyDispMap,...
    obj.FFTObj.linearizeInput,obj.FFTObj.gammaOffset);

t=toc;fprintf(['(' num2str(t) 's)\n']);

obj.rMod = rMod;

end