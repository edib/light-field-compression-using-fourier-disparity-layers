function FFTObj = ComputeFFT(obj,LFRef,varargin)

tic;fprintf('FFT Computation...');

FFTObj = FFT(LFRef,varargin{:});

t=toc;fprintf(['\b\b\b (done in ' num2str(t) 's)\n']);

obj.FFTObj = FFTObj;

end