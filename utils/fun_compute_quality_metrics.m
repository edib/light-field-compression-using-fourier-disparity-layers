function fun_compute_quality_metrics(RecDir,LFName,QPList)
%ENCODEFDL Summary of this function goes here
%   Detailed explanation goes here

tStart = tic;

%% Load reference
resuDir = fullfile(RecDir,LFName);
refDir = fullfile(resuDir,'Reference');
LFRef = LF.fromSubAps(refDir,'ext','png','name',LFName);

%% Compute quality metrics for each QP value
for QP = QPList
    %% Current directories
    predDir     = fullfile(resuDir,['Prediction_',num2str(QP)]);
    recDir      = fullfile(resuDir,['Reconstruction_',num2str(QP)]);
    resuMatPred = fullfile(resuDir,['quality_metrics_pred_',num2str(QP),'.mat']);
    resuMatRec  = fullfile(resuDir,['quality_metrics_rec_',num2str(QP),'.mat']);
    
    %% Prediction of current frame layer using Fourier Disparity Layers
    LFRec = LF.fromSubAps(predDir,'name',LFName,'ext','png');
    
    [psnr_y_view  ,psnr_y_mean  ,psnr_y_all  ,ssim_y_view  ,ssim_y_mean  ,...
     psnr_yuv_view,psnr_yuv_mean,psnr_yuv_all,ssim_yuv_view,ssim_yuv_mean] = ...
    LF.qualityMetrics(LFRec,LFRef);
    
    save(resuMatPred,...
        'psnr_y_view','psnr_y_mean'  ,'psnr_y_all'  ,'ssim_y_view'  ,'ssim_y_mean'  ,...
     'psnr_yuv_view' ,'psnr_yuv_mean','psnr_yuv_all','ssim_yuv_view','ssim_yuv_mean');
    
    LFRec = LF.fromSubAps(recDir,'name',LFName,'ext','png');
    
    [psnr_y_view  ,psnr_y_mean  ,psnr_y_all  ,ssim_y_view  ,ssim_y_mean  ,...
     psnr_yuv_view,psnr_yuv_mean,psnr_yuv_all,ssim_yuv_view,ssim_yuv_mean] = ...
     LF.qualityMetrics(LFRec,LFRef);
    
    save(resuMatRec,...
        'psnr_y_view','psnr_y_mean'  ,'psnr_y_all'  ,'ssim_y_view'  ,'ssim_y_mean'  ,...
     'psnr_yuv_view' ,'psnr_yuv_mean','psnr_yuv_all','ssim_yuv_view','ssim_yuv_mean');
end

toc(tStart)

end