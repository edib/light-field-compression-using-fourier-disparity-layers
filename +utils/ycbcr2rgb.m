function img = ycbcr2rgb(img,n,fullRange,conv2int)
%YCBCR2RGB Summary of this function goes here
%   Detailed explanation goes here

%  Recommendation ITU-R BT.709-6
M = [ 1.000000   0.000000  1.574800 ;
      1.000000  -0.187350 -0.468130 ;
      1.000000   1.855630  0.000000];

if (nargin < 2)
    dr = diff(getrangefromclass(img));
    n = log2(double(dr)+1);
else
    dr = double(2^n-1);
end

if (nargin < 3); fullRange = true; end
if (nargin < 4); conv2int = isinteger(img); end

if fullRange
    scaleY = 255; offsetY = 0 ; scaleC = 255; offsetC = 128;
else
    scaleY = 219; offsetY = 16; scaleC = 224; offsetC = 128;
end

img        = double(img);
img(:,:,1) = (255*img(:,:,1)-dr*offsetY)/scaleY;
img(:,:,2) = (255*img(:,:,2)-dr*offsetC)/scaleC;
img(:,:,3) = (255*img(:,:,3)-dr*offsetC)/scaleC;
img          = reshape(reshape( img,[],3 )* M' ,size(img));

if conv2int
    if(n<=8)
        img = uint8(img);
    elseif (n<=16)
        img = uint16(img);
    elseif (n<=32)
        img = uint32(img);
    elseif (n<=64)
        img = uint64(img);
    else
        print('invalid bit depth')
    end
end

end