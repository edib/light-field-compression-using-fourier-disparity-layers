function convname = params2name(name,imgSize,imgRes,subSamp,bitDepth,ext)
%PARAMS2NAME Summary of this function goes here
%   Detailed explanation goes here

imgSize(end+1:2) = 1;
imgRes (end+1:2) = 1;

imgSize = [num2str(imgSize(2)),'x',num2str(imgSize(1))];
imgRes  = arrayfun(@num2str,imgRes,'UniformOutput',false);
imgRes  = strjoin(imgRes,'x');
bitDepth = num2str(bitDepth);

convname = [strjoin({name,imgRes,imgSize,subSamp,bitDepth},'_'),['bit.',ext]];

end