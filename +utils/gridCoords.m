function [X,Y,Xq,Yq,varargout] = gridCoords(X,Y,Xq,Yq,varargin)
%GRIDCOORDS Summary of this function goes here
%   Detailed explanation goes here

I = (1:numel(X))';
offset = 1e-2;

p = inputParser();
p.addOptional('I'     , I     , @isnumeric);
p.addOptional('offset', offset, @isnumeric);

p.parse(varargin{:});
I      = p.Results.I;
offset = p.Results.offset;

sz = size(Xq);

ind = nan(numel(Xq),1);
b   = nan(numel(Xq),3);
nanmask = true(numel(Xq),1);

[dX,dY] = ndgrid([-offset,0,offset]);
sigma = [5,4,6,2,8,1,3,7,9];

for r = 1:numel(sigma)
    dx = dX(sigma(r));
    dy = dY(sigma(r));
    
    t = utils.triangulation(X+dx,Y+dy,I);
    [ind(nanmask),b(nanmask,:)] = t.pointLocation(Xq(nanmask)+dx,Yq(nanmask)+dy);
    nanmask = isnan(ind);
end

ind = reshape(ind,sz);
u = reshape(b(:,2),sz);
v = reshape(b(:,3),sz);

nanind = isnan(ind);
ind(nanind) = 1;
ut = ~mod(ind,2);
ind(nanind) = nan;

[x,y] = ind2sub(size(X)-1,ceil(ind/2));

u(ut) = 1-u(ut);
v(ut) = 1-v(ut);

Xq = x+u;
Yq = y+v;

[X,Y] = ndgrid(1:size(X,1),1:size(X,2));

if nargout>0; varargout{1} = u;   end
if nargout>1; varargout{2} = v;   end
if nargout>2; varargout{3} = x;   end
if nargout>3; varargout{4} = y;   end
if nargout>4; varargout{5} = ind; end
if nargout>5; varargout{6} = t;   end
if nargout>6; varargout{7} = I;   end

end