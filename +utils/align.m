function [Cols,Offs,varargout] = align(Cols,Offs,varargin)
%ALIGN Summary of this function goes here
%   Detailed explanation goes here

nvararg = nargin-2;
numLab = numel(Cols);
Maxs = cell(size(Cols));

filter = logical([1,1,0,0,0]);

for lab = 1:numLab
    if ~isempty(Cols{lab})
        break
    end
end

Off = Offs{lab};
numDim = numel(Off);
Max = size(Cols{lab});
Max(end+1:numDim) = 1;
Max = Max+Off;

for lab = 1:numLab
    if isempty(Cols{lab})
        continue
    end
    
    Maxs{lab} = size(Cols{lab});
    Maxs{lab}(end+1:numDim) = 1;
    Maxs{lab} = Maxs{lab} + Offs{lab};
    
    Off = min(Offs{lab},Off);
    Max = max(Maxs{lab},Max);
end

varargout = varargin;

for lab = 1:numLab
    if isempty(Cols{lab})
        continue
    end
    
    padPre  = Offs{lab}-Off;
    padPost = Max-Maxs{lab};
    
    padPre (~filter) = 0;
    padPost(~filter) = 0;
    
    Cols{lab} = padarray(Cols{lab},padPre ,nan,'pre' );
    Cols{lab} = padarray(Cols{lab},padPost,nan,'post');
    
    for it = 1:nvararg
        varargout{it}{lab} = padarray(varargout{it}{lab},padPre ,nan,'pre' );
        varargout{it}{lab} = padarray(varargout{it}{lab},padPost,nan,'post');
    end
    
    Offs{lab} = Off;
end
end