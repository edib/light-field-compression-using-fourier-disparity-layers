function [codebook,allCodebook] = lloyds(samples,nbins,numIter)
%LLOYDS Summary of this function goes here
%   Detailed explanation goes here

ndim = nnz(size(samples)~=1);
switch ndim
    case 1
        [~,codebook,~] = histcounts(samples,nbins);
    otherwise
        codebook = randn(ndim,nbins);
end

allCodebook = codebook;

for it = 1:numIter
    disp(it)
    [codebook,~,~] = dspvqdemomex(samples, codebook);
    allCodebook = [allCodebook;codebook];
end

end