function printLine(msg,varargin)
%PRINTLINE Summary of this function goes here
%   Detailed explanation goes here

persistent sz
if isempty(sz); sz = 0; end
if nargin>1; sz = varargin{1}; end

fprintf(repmat('\b',1,sz));
fprintf(msg);    
sz = numel(msg)-count(msg,'\n');

end