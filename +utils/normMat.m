function [A,rA] = normMat(A)
%NORMMAT Summary of this function goes here
%   Detailed explanation goes here

rA = [min(A(:)) max(A(:))];
A = (A-rA(1))/diff(rA);
end