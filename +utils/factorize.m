function [B,C,U,S,V] = factorize(M,k,Mask)
%FACTORIZE Summary of this function goes here
%   Detailed explanation goes here

if ~exist('Mask','var'); Mask = isnan(M); end
M(Mask) = 0;

[U,S,V] = utils.svd_nsq(M);
U = -U;
V = -V;
B = U(:,1:k)*S(1:k,1:k);
C = V(:,1:k)';

B(any(Mask,2),:) = nan;

end