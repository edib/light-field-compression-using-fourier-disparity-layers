function [Value,gv] = tighten(Value,varargin)
%TIGHTEN Summary of this function goes here
%   Detailed explanation goes here

if nargin>1
    Mask = varargin{1};
elseif islogical(Value)
    Mask = Value;
else
    Mask = ~isnan(Value);
end

numdims = ndims(Mask);
gv = cell(1,numdims);
for dim = 1:numdims
    M = any(Mask,setdiff(1:numdims,dim));
    gv{dim} = find(M,1,'first'):find(M,1,'last');
end

Value = Value(gv{:});

end