function [nbBits,peaksnr] = decode(varargin)
%DECODE Summary of this function goes here
%   Detailed explanation goes here

HEVCDir = HEVC.Dir;
if ispc
    TAppDecoder = fullfile(HEVCDir,'bin','TAppDecoder.exe');
elseif isunix
    TAppDecoder = fullfile(HEVCDir,'bin','TAppDecoderStatic');
elseif ismac
    error('custom HEVC not compiled for macOS');
else
    disp('Platform not supported')
end

p = inputParser; p.KeepUnmatched = true; p.StructExpand = true;

p.addParameter('BitstreamFile'      ,'bit.hevc',@ischar);
p.addParameter('ReconFile'          ,'rec.yuv' ,@ischar);
p.addParameter('SkipFrames'         ,'0'       ,@ischar);
p.addParameter('WarnUnknowParameter','1'       ,@ischar);
p.addParameter('LogFile'            ,'log.rtf' ,@ischar);

% Add additional default parameters here

p.parse(varargin{:});

results = p.Results;
parameters = fieldnames (results);
values     = struct2cell(results);

LogFile = p.Results.LogFile;

argList = paramToArg(parameters,values);
command = [TAppDecoder,' ',argList];
disp(command)
status = system(command);
if status, error('HEVC Error'); end

[nbBits,peaksnr] = HEVC.parseLog(LogFile);
end

function argList = paramToArg(parameters,values)
argList = cellfun(@(param,val) ['--',param,'="',val,'"'],...
    parameters,values,'UniformOutput',false);
argList = strjoin(argList(:));
end