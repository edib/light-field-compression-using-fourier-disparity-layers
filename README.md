
# Light Field Compression using Fourier Disparity Layers

## Description

This is the code used in E. Dib, M. Le Pendu and C. Guillemot, "Light Field Compression using Fourier Disparity Layers", ICIP 2019.

## Citation

If you find this code useful, please consider citing our work:

``` bibtex
@inproceedings{Dib2019,
author = {Dib, Elian and Le Pendu, Mikael and Guillemot, Christine},
booktitle = {2019 IEEE International Conference on Image Processing (ICIP)},
doi = {10.1109/ICIP.2019.8803756},
isbn = {978-1-5386-6249-6},
keywords = {Compression,FDL,FFT,Fourier,Light field},
month = {sep},
pages = {3751--3755},
publisher = {IEEE},
title = {{Light Field Compression Using Fourier Disparity Layers}},
url = {https://ieeexplore.ieee.org/document/8803756/},
year = {2019}
}
```

## Software

The code has been developed and tested using Matlab 2018a on Windows 10 and Ubuntu 20.04.

It makes use of the FDL Toolbox available [here](https://github.com/LEPENDUM/FDL-Toolbox), already included in this repository.

## Launch the algorithm

The compression algorithm presented in our paper has been implemented in **fun_encode_fdl.m**:

You may use this function by specifying the input and output directories **RefDir** and **RecDir**, the light field name **LFName**, the angular ranges **uRange** and **vRange**, the image border crop **crop**, the layer configuration **OrderName** and the HEVC quantization parameters **QPList**.

Additional parameters, such as the Fourier Disparity Layers parameters may additionally be specified, otherwise the default values are used.

A convenience Matlab script **script_encode_fdl.m** is provided to launch the compression for different light field in **RefDir**. Make sure to set the appropriate values for **RefDir**

``` matlab
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input and output directories  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RefDir = '~/LightFields/LightFields_RGB';
RecDir = '~/FDL_Results';
```

and the Layer configurations

``` matlab
%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layer Configurations %
%%%%%%%%%%%%%%%%%%%%%%%%%
OrderNames = {'Circular_2','Hierarchical_2'}; % 2 Layers
```

## Light field directory structure

The algorithm expects the following directory structure to read a light field from a collection of images:

``` txt
+ RefDir
| + LFName1
| | LF_Name1_1_1.png
| | LF_Name1_1_2.png
| |        ...
| | LF_Name1_15_14.png
| | LF_Name1_15_15.png
| + LFName2
|     ...
```

where **RefDir** is a directory containing different folders for light fields **LFName1** and **LFName2** and each sub-folder **LFName** should contain images with the name format **LFName_v_u.png** where **u** and **v** stand for the angular coordinates.

## Layer configurations

Different Layer configurations are provided in the folder **LayerConfigurations** including the **Circular 2**, **Hierarchical 2**, **Circular 4** and **Hierarchical 4** presented in our paper.

## Save and load custom Layer configurations

WIP

## Fourier Disparity Layers

You may specify the number of layers **numLayers** and the regularization parameters **lambdaCalib** and **lambdaConstruct** respectively for the FDL calibration and FDL computation.

## Save and load custom FDL parameters

WIP

## HEVC executables

**HEVC HM 16.10** executables **TAppEncoderStatic** and **TAppEncoder.exe** are provided respectively for Linux and Windows in the folder **+HEVC/bin** and are necessary for the correct execution of the algorithm thus you should make sure they are executable prior to running the algorithm.

This can be done for instance on linux-based systems using the following command, provided you have the right permissions:

``` bash
chmod +x +HEVC/bin/TAppEncoderStatic
```

Alternatively, you may compile HEVC from source and place the resulting executable in the folder **+HEVC/bin**
