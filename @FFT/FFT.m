classdef FFT < handle
    %FDLOBJ Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        linearizeInput
        gammaOffset
        
        fullResX
        fullResY
        padSizeX
        padSizeY
        
        wx
        wy
        ViewsFFT
    end
    
    methods
        function obj = FFT(LFRef,varargin)
            p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
            p.addParameter('linearizeInput' , false, @islogical);
            p.addParameter('gammaOffset'    , 0    , @isnumeric);
            
            p.parse(varargin{:});
            obj.linearizeInput = p.Results.linearizeInput;
            obj.gammaOffset    = p.Results.gammaOffset;
            
            [obj.ViewsFFT,obj.wx,obj.wy,obj.fullResX,obj.fullResY,...
                obj.padSizeX,obj.padSizeY,obj.linearizeInput,obj.gammaOffset] = ...
                FFT.computeFFT(LFRef,varargin{:},...
                'linearizeInput',obj.linearizeInput,'gammaOffset',obj.gammaOffset);
        end
    end
    
    methods
        function LFRef = getViews(obj,varargin)
            
            LFRef = FFT.computeViews(obj.ViewsFFT,...
                'padSizeX',obj.padSizeX,'padSizeY',obj.padSizeY,...
            'linearizeInput',obj.linearizeInput,'gammaOffset',obj.gammaOffset,varargin{:});
        end
    end
    
    methods(Static)
        [ViewsFFT,wx,wy,fullResX,fullResY,padSizeX,padSizeY,...
            linearizeInput,gammaOffset] = computeFFT(LFRef,varargin);
        LFRef = computeViews(ViewsFFT,varargin)
    end
end