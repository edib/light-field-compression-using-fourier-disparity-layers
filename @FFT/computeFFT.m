function [ViewsFFT,wx,wy,fullResX,fullResY,padSizeX,padSizeY,...
    linearizeInput,gammaOffset] = computeFFT(LFRef,varargin)
%CONVERTToFOURIER Summary of this function goes here
%   Detailed explanation goes here

% Create input parser scheme
p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('linearizeInput' , false       , @islogical);
p.addParameter('gammaOffset'    , 0           , @isnumeric);
p.addParameter('padType'        , 'symmetric' , @ischar);
p.addParameter('padSize'        , [15,15]     , @isnumeric);
p.addParameter('Windowing'      , true        , @islogical);
p.addParameter('windowProfile'  , 'hann'      , @ischar);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Input Light Field parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

p.parse(varargin{:});
linearizeInput = p.Results.linearizeInput; %Use inverse gamma correction to process images in linear space.
gammaOffset    = p.Results.gammaOffset;    %Offset applied before inverse gamma correction.
padType        = p.Results.padType;        %Padding type, 'replicate' or 'symmetric'
padSize        = p.Results.padSize;        %Window profile type, 'hann' or 'linear'
Windowing      = p.Results.Windowing;	   %Use window to decrease intensity of padded pixels.
windowProfile  = p.Results.windowProfile;  %Window profile type, 'hann' or 'linear'

useGPU = parallel.gpu.GPUDevice.isAvailable; %Use gpu for initilisation (Windowing + Fourier Transform of input views).

padSizeX=padSize(2); %Number of padded pixels on left and right borders.
padSizeY=padSize(1); %Number of padded pixels on top and bottom borders.

sz = size(LFRef);
sz = sz(4:end);
InitViewIds = 1:prod(sz); %Selected views for the input of the FDL construction.
numInitViews = length(InitViewIds);

%% Prepare variables
if(linearizeInput)
    LFRef = BT709_gammaDecode(LFRef+gammaOffset);
end

imgSizeY = size(LFRef,1);
imgSizeX = size(LFRef,2);
nChan = size(LFRef,3);

fullResX = padSizeX*2+imgSizeX;
fullResY = padSizeY*2+imgSizeY;

[wx,wy,~,~] = GenerateFrequencyGrid(fullResX,fullResY);

%window profile:
if(Windowing)
    Window = single(GenerateWindowIntensity(fullResX,fullResY,padSizeX,padSizeY,windowProfile));
    if(useGPU), Window = gpuArray(Window);end
end

%% Pre-process input data (Padding/Windowing + Fourier Tramsform )
ViewsFFT = zeros([numInitViews,nChan,fullResY,fullResX],'single');
for l=1:numInitViews
    idView = InitViewIds(l);
    
    if(useGPU)
        orgPad = gpuArray(padarray(single(LFRef(:,:,:,idView)),[padSizeY padSizeX],padType));
    else
        orgPad = padarray(single(LFRef(:,:,:,idView)),[padSizeY padSizeX],padType);
    end
    if(Windowing)
        orgPad = bsxfun(@times, orgPad, Window);
    end
    
    if(useGPU)
        ViewsFFT(l,:,:,:) = gather(permute(fftshift(fftshift(fft2(orgPad),1),2),[3 1 2]));
    else
        for ch=1:nChan, ViewsFFT(l,ch,:,:) = fftshift(fft2(orgPad(:,:,ch))); end
    end
end

end