addpath(genpath('./FDL-Toolbox'));
addpath('./utils');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Input Light Field parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FFTParams.gpuInit = true;         %true -> use gpu for initilisation (Windowing + Fourier Transform of input views).
FFTParams.padType = 'symmetric';  %padding type, 'replicate' or 'symmetric'
FFTParams.padSize = [15,15];      %padding size
FFTParams.Windowing = true;	      %use window to decrease intensity of padded pixels.
FFTParams.windowProfile = 'hann'; %type of window intensity profile
FFTParams.linearizeInput = false; %use inverse gamma correction to process images in linear space.
FFTParams.gammaOffset = 0;	      %offset applied before inverse gamma correction.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Default Parameters for FDL calibration + construction %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FDLParams.numLayers = 30;
FDLParams.lambdaCalib = 1;
FDLParams.lambdaConstruct = .1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  HEVC Quantization Parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
QPList = [40,30,20,15,10,5];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input and output directories  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RefDir = '~/LightFields/LightFields_RGB';
RecDir = '~/FDL_Results';

%%%%%%%%%%%%%%%%%%%%%%%%%
%% Layer Configurations %
%%%%%%%%%%%%%%%%%%%%%%%%%
OrderNames = {'Circular_2','Hierarchical_2'}; % 2 Layers
% OrderNames = {'Circular_4','Hierarchical_4'}; 4 Layers

%%
FlippedCoords = {'Fountain_&_Vincent_2','Greek'};
HCI = {'StillLife','Butterfly','Buddha','Greek','Sideboard'};

%% Parse light fields in directory
s = dir(RefDir);
s = s([s.isdir]);
LFNames = {s(3:end).name};

%% Run algorithm for each light field
for LFName = LFNames
    LFName = LFName{:};
    
    % Fix calibration by flipping angular coordinates
    switch LFName
        case FlippedCoords
            FDLParams.flipU = true;
            FDLParams.flipV = false;
        otherwise
            FDLParams.flipU = false;
            FDLParams.flipV = false;
    end
    
    % Specify angular indices and spatial crop
    switch LFName
        case HCI
            uRange = 1:9;
            vRange = 1:9;
            crop = [0 0 0 0];
        otherwise %Lytro
            uRange = 4:12;
            vRange = 4:12;
            crop = [4 5 5 5];
    end
    
    FDLParams.nU = length(uRange);
    FDLParams.nV = length(vRange);
    
    %% Encode U,V,D
    % WIP
    
    %% Run algorithm for each configuration
    for OrderName = OrderNames
        OrderName = OrderName{:};
        
        % Try to use optimized values for lambdaCalib and lambdaConstruct parameters
        try
            folder_name = ['./FDL_Params_',num2str(FDLParams.numLayers)];
            file_name = [LFName,'_',OrderName,'.mat'];
            m = matfile(fullfile(folder_name,file_name));
            
            FDLParams.lambdaCalib = m.lambdaCalibFull;
            FDLParams.lambdaConstruct = m.lambdaConstructFull;
        catch
            warning(['Could not find optimized parameters in ',folder_name]);
            warning('Using default parameters');
        end
        
        %% Run algorithm
        fun_encode_fdl(RefDir,fullfile(RecDir,OrderName),LFName,uRange,vRange,crop,OrderName,QPList,FFTParams,FDLParams);
        fun_compute_quality_metrics(fullfile(RecDir,OrderName),LFName,QPList);
    end
end